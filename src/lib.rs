/*
   Copyright 2020 Bota Viorel

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

pub mod definitions
{
    //TODO initiate these values from storage
    pub struct Session;

    impl Session {
        #[cfg(debug_assertions)]
        pub(crate) const fn key_length() -> usize { 2 }

        #[cfg(not(debug_assertions))]
        pub(crate) const fn key_length() -> usize { 7 }
    }

    pub struct Symbols;

    impl Symbols
    {
        pub(crate) const fn alive_message() -> u8
        {
            0x06
        }

        pub(crate) const fn message_separator() -> u8
        {
            0x0D
        }

        pub(crate) const fn data_separator() -> u8
        {
            0x02
        }
    }
}

pub mod session_key
{
    use rand::Rng;
    use crate::definitions::Session;

    pub struct SessionKey;

    impl SessionKey {
        pub fn new() -> Vec<u8>
        {
            rand::thread_rng().gen::<[u8; SessionKey::length()]>().to_vec()
        }

        pub(crate) const fn length() -> usize
        {
            Session::key_length()
        }
    }
}

pub trait Validation
{
    fn is_valid_from(data: Vec<u8>) -> bool;
}

pub trait Markers
{
    fn get_start_symbol() -> Vec<u8>;
}

pub mod authentication_message
{
    use crate::data_separator::DataSeparator;

    pub struct AuthenticationMessage;

    impl AuthenticationMessage {
        //TODO update security token
        pub fn new_with_key(mut session_key: Vec<u8>) -> Vec<u8>
        {
            let mut new_message = "Security token".as_bytes().to_vec();
            new_message.push(DataSeparator::VALUE);
            new_message.append(&mut session_key);
            new_message
        }
    }
}

pub mod alive_message
{
    use crate::{Markers, Validation};
    use crate::definitions::Symbols;
    use crate::message_separator::MessageSeparator;

    pub struct AliveMessage;

    impl Markers for AliveMessage {
        fn get_start_symbol() -> Vec<u8> {
            vec![Symbols::alive_message()]
        }
    }

    impl AliveMessage {
        pub fn is_valid_from(data: Vec<u8>, session_key: &Vec<u8>) -> bool {

            let start_symbol = AliveMessage::get_start_symbol();
            let start_of_key = start_symbol.len();
            let end_of_key = start_of_key+session_key.len();

            if end_of_key > data.len() {
                return false;
            }

            if !MessageSeparator::is_valid_from(data[end_of_key..].to_vec()) {
                return false;
            }

            if &data[..start_of_key].to_vec() != &start_symbol {
                return false;
            }

            if &data[start_of_key..end_of_key].to_vec() == session_key {
                return true;
            }

            return false;
        }
    }
}

#[cfg(test)]
mod alive_message_tests {
    use crate::alive_message::AliveMessage;
    use crate::session_key::SessionKey;
    use crate::Markers;
    use crate::message_separator::MessageSeparator;

    #[test]
    fn validation_of_valid_message() {
        let session_key = SessionKey::new();
        let mut alive_message = vec![];
        alive_message.extend(AliveMessage::get_start_symbol());
        alive_message.extend(session_key.clone());
        alive_message.push(MessageSeparator::VALUE);

        assert!(AliveMessage::is_valid_from(alive_message, &session_key));
    }

    #[test]
    fn validation_message_missing_start() {
        let session_key = SessionKey::new();
        let mut alive_message = vec![];
        alive_message.extend(session_key.clone());
        alive_message.push(MessageSeparator::VALUE);

        assert!(!AliveMessage::is_valid_from(alive_message, &session_key));
    }

    #[test]
    fn validation_message_missing_end() {
        let session_key = SessionKey::new();
        let mut alive_message = vec![];
        alive_message.extend(AliveMessage::get_start_symbol());
        alive_message.extend(session_key.clone());

        assert!(!AliveMessage::is_valid_from(alive_message, &session_key));
    }

    #[test]
    fn validation_missing_key() {
        let session_key = SessionKey::new();
        let mut alive_message = vec![];
        alive_message.extend(AliveMessage::get_start_symbol());
        alive_message.push(MessageSeparator::VALUE);

        assert!(!AliveMessage::is_valid_from(alive_message, &session_key));
    }

    #[test]
    fn validation_wrong_key_value() {
        let session_key = SessionKey::new();
        let mut alive_message = vec![];
        alive_message.extend(AliveMessage::get_start_symbol());
        let mut wrong_key = session_key.clone();
        let last_key_byte = wrong_key.pop().unwrap();
        wrong_key.push(last_key_byte + 1);
        alive_message.extend(wrong_key);
        alive_message.push(MessageSeparator::VALUE);

        assert!(!AliveMessage::is_valid_from(alive_message, &session_key));
    }

    #[test]
    fn validation_message_key_to_short() {
        let session_key = SessionKey::new();
        let mut alive_message = vec![];
        alive_message.extend(AliveMessage::get_start_symbol());
        let mut wrong_key = session_key.clone();
        wrong_key.pop();
        alive_message.extend(wrong_key);
        alive_message.push(MessageSeparator::VALUE);

        assert!(!AliveMessage::is_valid_from(alive_message, &session_key));
    }

    #[test]
    fn validation_message_key_to_long() {
        let session_key = SessionKey::new();
        let mut alive_message = vec![];
        alive_message.extend(AliveMessage::get_start_symbol());
        let mut wrong_key = session_key.clone();
        wrong_key.push(0x01);
        alive_message.extend(wrong_key);
        alive_message.push(MessageSeparator::VALUE);

        assert!(!AliveMessage::is_valid_from(alive_message, &session_key));
    }

    #[test]
    fn validation_empty_message() {
        let session_key = SessionKey::new();
        let alive_message = vec![];

        assert!(!AliveMessage::is_valid_from(alive_message, &session_key));
    }

    #[test]
    fn validation_empty_key() {
        let session_key = SessionKey::new();
        let mut alive_message = vec![];
        alive_message.extend(AliveMessage::get_start_symbol());
        alive_message.extend(session_key.clone());
        alive_message.push(MessageSeparator::VALUE);

        assert!(!AliveMessage::is_valid_from(alive_message, &vec![]));
    }

    #[test]
    fn validation_key_to_short() {
        let mut session_key = SessionKey::new();
        let mut alive_message = vec![];
        alive_message.extend(AliveMessage::get_start_symbol());
        alive_message.extend(session_key.clone());
        alive_message.push(MessageSeparator::VALUE);

        session_key.pop();
        assert!(!AliveMessage::is_valid_from(alive_message, &session_key));
    }

    #[test]
    fn validation_key_to_long() {
        let session_key = SessionKey::new();
        let mut alive_message = vec![];
        alive_message.extend(AliveMessage::get_start_symbol());
        alive_message.extend(session_key.clone());
        alive_message.push(MessageSeparator::VALUE);

        let mut wrong_key = session_key.clone();
        wrong_key.push(0x55);
        assert!(!AliveMessage::is_valid_from(alive_message.clone(), &wrong_key));

        wrong_key = session_key.clone();
        wrong_key.extend(vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
        assert!(!AliveMessage::is_valid_from(alive_message, &wrong_key));
    }
}

pub mod generic_message
{
    pub struct GenericMessage
    {
        pub content: Vec<u8>
    }

    pub trait MessageOperations
    {
        fn add(&mut self, data_segment: u8);
        fn clone(&self) -> Self;
    }

    impl MessageOperations for GenericMessage
    {
        fn add(&mut self, data_segment: u8)
        {
            self.content.push(data_segment);
        }

        fn clone(&self) -> Self
        {
            GenericMessage { content: self.content.clone() }
        }
    }
}

pub mod message_separator
{
    use crate::definitions::Symbols;
    use crate::Validation;

    pub struct MessageSeparator;

    impl MessageSeparator {
        pub(crate) const VALUE: u8 = Symbols::message_separator();
    }

    impl Validation for MessageSeparator {
        fn is_valid_from(symbol: Vec<u8>) -> bool {
            if symbol.len() > vec![MessageSeparator::VALUE].len() {
                return false;
            }

            match symbol.first() {
                Some(&data) => data == MessageSeparator::VALUE,
                None => false
            }
        }
    }
}

#[cfg(test)]
mod message_separator_tests {
    use crate::Validation;
    use crate::message_separator::MessageSeparator;

    #[test]
    fn validation_of_valid_symbol() {
        let symbol = vec![MessageSeparator::VALUE];

        assert!(MessageSeparator::is_valid_from(symbol));
    }

    #[test]
    fn validation_empty_input() {
        let symbol = vec![];

        assert!(!MessageSeparator::is_valid_from(symbol));
    }

    #[test]
    fn validation_empty_input_to_long() {
        let symbol = vec![MessageSeparator::VALUE, 0x0];

        assert!(!MessageSeparator::is_valid_from(symbol));
    }
}

pub mod data_separator
{
    use crate::definitions::Symbols;

    pub(crate) struct DataSeparator;

    impl DataSeparator {
        pub(crate) const VALUE: u8 = Symbols::data_separator();
    }
}
