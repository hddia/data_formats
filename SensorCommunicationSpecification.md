# Bluetooth communication procedure
Version 1.0.0

The Server will attempt to establish a Bluetooth connection (RFCOMM) with the sensor using the sensor's network address.

The sensor's network address will be introduced in the PC by a manual operator.


# After establishing the RFCOMM connection:

*  Step1:     
    * Within SERVER_TIMEOUT_T1 after the communication was established, the Server will send to the Sensor the AUTHENTICATION_MESSAGE
    * If the Sensor will not receive the AUTHENTICATION_MESSAGE within SERVER_TIMEOUT_T1 then the Sensor will close the current connection.
*  Step2:
    * If the Sensor receives a valid AUTHENTICATION_MESSAGE within SERVER_TIMEOUT_T1, the Sensor will reply to the Server with an ALIVE_MESSAGE within SENSOR_TIMEOUT_T1 from the moment it received the AUTHENTICATION_MESSAGE
    * If the Server will not receive the ALIVE_MESSAGE within SENSOR_TIMEOUT_T1 then the Server will close the current connection
*  Step3:     
    *  The Server will wait for messages from the Sensor
    *  The Sensor will send, as needed, to the Server one of the following: ACCELEROMETER_DATA, GYROSCOPE_DATA, SUOND_DATA


## Definitions:
*     AUTHENTICATION_MESSAGE - a series of bytes composed from: 
    *     PUBLIC_KEY
    *     TEXT_SEPARATION_SYMBOL
    *     SESSION_KEY
*     SERVER_TIMEOUT_T1 - 30[s]
*     PUBLIC_KEY - a pregenerated 256 byte long RSA public key
*     TEXT_SEPARATION_SYMBOL - one byte with the value 0x02
*     SESSION_KEY - 7 bytes with random generated values
*     SENSOR_TIMEOUT_T1 - 30[s]
*     ALIVE_MESSAGE - a series of bytes composed from: 
    *     ALIVE_MESSAGE_SYMBOL
    *     SESSION_KEY (the same key as the one received from the sensor in the AUTHENTICATION_MESSAGE)
    *     MESSAGE_SEPARATOR_SYMBOL
*     ALIVE_MESSAGE_SYMBOL - one byte with the value 0x06
*     MESSAGE_SEPARATOR_SYMBOL - one byte with the value 0x0D
*     ACCELEROMETER_DATA - a series of bytes composed from: 
    *     ACCELEROMETER_MESSAGE_SYMBOL
    *     X_VALUE - interger encoded over 2 bytes (TBD signed or unsgined)
    *     Y_VALUE - interger encoded over 2 bytes (TBD signed or unsgined)
    *     Z_VALUE - interger encoded over 2 bytes (TBD signed or unsgined)
    *     MESSAGE_SEPARATOR_SYMBOL
*     ACCELEROMETER_MESSAGE_SYMBOL - one byte with the value 0x41
*     GYROSCOPE_DATA - a series of bytes composed from: 
    *     GYROSCOPE_MESSAGE_SYMBOL
    *     X_VALUE - interger encoded over 2 bytes (TBD signed or unsgined)
    *     Y_VALUE - interger encoded over 2 bytes (TBD signed or unsgined)
    *     Z_VALUE - interger encoded over 2 bytes (TBD signed or unsgined)
    *     MESSAGE_SEPARATOR_SYMBOL
*     GYROSCOPE_MESSAGE_SYMBOL - one byte with the value 0x47
*     SUOND_DATA - a series of bytes composed from: 
    *     SOUND_MESSAGE_SYMBOL
    *     SOUND_DATA
    *     MESSAGE_SEPARATOR_SYMBOL
*     SOUND_MESSAGE_SYMBOL - one byte with the value 0x53
*     SOUND_DATA - interger encoded over 2 bytes (TBD signed or unsgined)
